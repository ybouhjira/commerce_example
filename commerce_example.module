<?php

/**
 * Copy-pasted from commerce_examples module (product_example/product_example.module)
 * ////////////////////////////////////////////////////////////////////////////
 * Create a product programmatically.
 *
 * This is stolen shamelessly from commerce_bpc. Thanks for the help here!
 *
 * @param $product_type
 *   (string) The name of the product type for which products should be created.
 * @param $price
 *   Decimal amount of price. If additional fields need to be populated they
 *   can be populated in exactly the same way as the commerce_price field.
 * @param $extras
 *   An array for the values of  'extra fields' defined for the product type
 *   entity, or patterns for these. Recognized keys are:
 *   - status
 *   - uid
 *   - sku
 *   - title
 *   Note that the values do NOT come in the form of complex arrays (as they
 *   are not translatable, and can only have single values).
 * @return
 *   The ID of the created product.
 */
function product_example_create_product($product_type, $price, $extras) {
    $form_state = array();
    $form_state['values'] = array();
    $form = array();
    $form['#parents'] = array();

    // Generate a new product object
    $new_product = commerce_product_new($product_type);

    $new_product->status = $extras['status'];
    $new_product->uid = $extras['uid'];

    $new_product->sku = $extras['sku'];
    $new_product->title = $extras['title'];
    $new_product->created = $new_product->changed = time();

    //commerce_price[und][0][amount]
    $price = array(LANGUAGE_NONE => array(0 => array(
                'amount' => $price * 100,
                'currency_code' => commerce_default_currency(),
    )));
    $form_state['values']['commerce_price'] = $price;

    // Notify field widgets to save their field data
    field_attach_submit('commerce_product', $new_product, $form, $form_state);

    commerce_product_save($new_product);
    return $new_product->product_id;
}

function commerce_example_menu() {
    return array(
        'custom_checkout' => array(
            'title' => t('Checkout'),
            'page callback' => 'commerce_example_checkout',
            'access arguments' => array('access content'),
        ),
    );
}

function commerce_example_checkout() {
    // Get the current user
    global $user;

    // Create the product
    $product_id = product_example_create_product('booking', 3500, array(
        'uid' => $user->uid,
        'title' => t('Booking'),
        'sku' => $user->uid . "_" . time(),
        'status' => TRUE,
            )
    );
    
    // Add the product
    commerce_cart_product_add_by_id($product_id);
    
    drupal_goto('cart');
    
}